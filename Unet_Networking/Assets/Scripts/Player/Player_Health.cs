﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Player_Health : NetworkBehaviour {

	[SyncVar(hook = "OnHealthChange")] public int health = 100;
    public int maxHealth = 100;
    public Text healthText;
	private bool shouldDie;
	public bool isDead = false;
	//Events!
	public delegate void DieDelegate();
	public event DieDelegate EventDie;
	public delegate void RespawnDelegate();
	public event DieDelegate EventRespawn;

    public override void OnStartLocalPlayer()
    {
        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        //health = maxHealth;
        //Debug.Log(gameObject.name + "Set Health UI");
        SetHealthText();
	}
	
	void Update () 
	{
		CheckCondition ();
	}

	void CheckCondition()
	{
		if(health <= 0 && !shouldDie && !isDead)
		{
			shouldDie = true;
		}

		if(health <= 0 && shouldDie)
		{
			if(EventDie != null)
			{
				EventDie();
			}
			shouldDie = false;// stops event right after one use
		}

		if(health > 0 && isDead)
		{
			if(EventRespawn !=null)
			{
				isDead = false;
				EventRespawn();//Respawn Event for other scripts to sub to.
			}
		}
	}

	void SetHealthText()
	{
		if(isLocalPlayer)
		{
            healthText.text = "Health" + health.ToString();
		}
	}

	void OnHealthChange(int hp)//hook
	{
        Debug.Log(this.gameObject.name + "-Health Change Hook-" + hp);
        health = hp; 
		SetHealthText ();
	}

	public void ApplyDamage(int dmg)
	{
        if (!isServer)
        {
            return;
        }

		health -= dmg;
		//Damage feedback
	}
	
    //can store this later for Vengance
	public void IGotHurtBy(string attacker)
	{
		//Debug.Log (this.gameObject.name.ToString () + " Was shot by" + attacker);
	}
   
    //respawn
    public void ResetHealth()
	{
		health = maxHealth;
	}
}
