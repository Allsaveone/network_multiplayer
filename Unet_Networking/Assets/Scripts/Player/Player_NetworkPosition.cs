﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

using System.Collections;

[NetworkSettings(channel = 0,sendInterval = 0.1f)] //attribute o adjust channelof info and sned rate
public class Player_NetworkPosition : NetworkBehaviour {
	[SyncVar(hook = "SyncPosValues")]
	private Vector3 syncPos;

	[SerializeField]private bool useHistoryLerp = false;
	private List<Vector3> syncPosList = new List<Vector3>();
	private float closeEnough = 0.1f;

	[SerializeField]Transform myTransform;
	private float lerpRate;
	private float normalLerpRate = 20f;
	private float fastLerpRate = 40f;

	
	private Vector3 lastPos;
	private float threshold = 0.5f;

	private NetworkClient client;
	private int latency;
	private Text latencyText;

	void Start()
	{
		client = GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ().client;
		latencyText = GameObject.Find ("LatencyText").GetComponent<Text> ();
		lerpRate = normalLerpRate;
	}

	void Update()
	{
		LerpPosition ();
		ShowLatency ();
	}

	void FixedUpdate () 
	{
		TransmitPosition ();
	}

	void LerpPosition()
	{
		if(!isLocalPlayer) //if its not mine, smooth its movement.
		{
			if(useHistoryLerp)
			{
				HistoryLerp();
			}
			else
			{
				GenericLerp();
			}
		}
	}

	[Command]//sends to server
	void CmdProvidePositionToServer(Vector3 pos)
	{
		syncPos = pos;
		//Debug.Log ("CmdProvidePositionToServer");
	}

	[ClientCallback]//
	void TransmitPosition()
	{
		if(isLocalPlayer)
		{
			if(Vector3.Distance(myTransform.position,lastPos) > threshold)//problem here with &&
			{
				CmdProvidePositionToServer (myTransform.position);
				lastPos = myTransform.position;
			}

//			CmdProvidePositionToServer (myTransform.position);
//			lastPos = myTransform.position;
		}
	}

	[Client]
	void SyncPosValues(Vector3 latestPos) //recieves hook function
	{
		syncPos = latestPos;
		syncPosList.Add (syncPos);
		Debug.Log ("Hook");
	}

	void ShowLatency()
	{
		if(isLocalPlayer)
		{
			latency = client.GetRTT();
			latencyText.text = "Ping" + latency.ToString() + "ms";
		}
	}

	void GenericLerp()
	{
		myTransform.position = Vector3.Lerp (myTransform.position,syncPos, Time.deltaTime*lerpRate);
	}

	void HistoryLerp()
	{
		//Debug.Log ("HistoryLerp");

		if(syncPosList.Count > 0)
		{
			myTransform.position = Vector3.Lerp (myTransform.position,syncPosList[0], Time.deltaTime*lerpRate);

			if(Vector3.Distance(myTransform.position,syncPosList[0]) < closeEnough)
			{
				syncPosList.RemoveAt(0);
			}

			if(syncPosList.Count  > 10)
			{
				lerpRate = fastLerpRate;
				Debug.Log ("Fast");
			}
			else
			{
				lerpRate = normalLerpRate;
				Debug.Log ("Normal");
			}

			Debug.Log (syncPosList.Count.ToString());

		}
	}

}
