﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player_SetupNetwork : NetworkBehaviour {

	//public GameObject sceneCamera;
	[SerializeField]Camera firstPersonCamera;
	[SerializeField]AudioListener firstPersonListener;
    [SerializeField]NetworkAnimator netAnim;
    //Controller Here

	// Use this for initialization
	public override void OnStartLocalPlayer() 
	{	
		GameObject sceneCam = GameObject.Find("SceneCamera");
        //sceneCam.tag = "Untagged";
        sceneCam.SetActive(false);

        //GetComponent<Character_Animator>().enabled = true;
        GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        firstPersonCamera.tag = "MainCamera";
	    firstPersonCamera.enabled = true;
		firstPersonListener.enabled = true;

        Renderer[] rends = GetComponentsInChildren<Renderer>();

        foreach (Renderer ren in rends)
        {
            ren.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        }

        //Send if i am local
        netAnim.SetParameterAutoSend(0, true);
        netAnim.SetParameterAutoSend(1, true);
        netAnim.SetParameterAutoSend(2, true);

        //GetComponent<Network_Chat>().enabled = true;
    }

    public override void PreStartClient()//Send if the Go is a client
    {
        netAnim.SetParameterAutoSend(0, true);
        netAnim.SetParameterAutoSend(1, true);
        netAnim.SetParameterAutoSend(2, true);
    }

}
