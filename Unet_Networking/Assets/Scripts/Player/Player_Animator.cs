﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player_Animator : NetworkBehaviour {

    public Animator anim;

    [SerializeField]public bool useIk = false;
    [SerializeField] public Transform aimObj;
    [SerializeField]public Vector3 aimOffset;

    public float lookIKWeight = 0;
    public float bodyIKWeight = 0;
    public float headIKWeight = 0;
    public float clampWeight = 0;

    public bool moving = false;

    void Start () {
	
	}

    void Update()
    {
        if (isLocalPlayer)
        {
            PlayerAnimatorInput();
        }      
    }

    void PlayerAnimatorInput()
    {
        float speed = Input.GetAxisRaw("Vertical") * 2.5f;
        float direction = Input.GetAxisRaw("Horizontal") * 2.5f;
        //Debug.Log(speed + "/" + direction);

        if (speed == 0 && direction == 0)
        {
            moving = false;
        }
        else
        {
            moving = true;
        }

        anim.SetFloat("Speed", speed);
        anim.SetFloat("Direction", direction);
        anim.SetBool("Moving", moving);
    }

    void OnAnimatorIK(int layerIndex)
    {
        //Debug.Log("!");
        if (useIk)
        {

            if (aimObj != null)
            {
                anim.SetLookAtWeight(lookIKWeight, bodyIKWeight, headIKWeight, 0, clampWeight);
                Vector3 aim = aimObj.position + aimOffset;
                anim.SetLookAtPosition(aim);
            }
            /*
            //LeftHand
            anim.SetIKPosition(AvatarIKGoal.LeftHand, aimObj.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, aimObj.rotation);

            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, bodyIKWeight);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, bodyIKWeight);
            */
        }

    }
}
