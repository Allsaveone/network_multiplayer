﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Player_Death : NetworkBehaviour {

	[SerializeField]Camera firstPersonCamera;
	[SerializeField]AudioListener firstPersonListener;
	[SerializeField]GameObject firstPersonCharacter;

	private Player_Health hp;
	private GameObject player_UI;

    public override void PreStartClient()//set references that are also req for remote clients
    {
        hp = GetComponent<Player_Health>();
        hp.EventDie += DisablePlayer; //Sub to event!
    }

    public override  void OnStartLocalPlayer() 
	{
        player_UI = GameObject.Find("PlayerUI").gameObject;
    }

    public override void OnNetworkDestroy()
    {
		hp.EventDie -= DisablePlayer;
	}

	void DisablePlayer () 
	{
		Debug.Log ("DisablePlayer");
		GetComponent<CharacterController>().enabled = false;
		GetComponent<Player_Network_Shoot>().enabled = false;
		GetComponent<CapsuleCollider>().enabled = false;
        //GetComponent<Player_Animator>().enabled = false;

        //		firstPersonCamera.enabled = false;
        //		firstPersonListener.enabled = false;
        firstPersonCharacter.SetActive (false);

		hp.isDead = true;
//		hp.enabled = false;

		if(isLocalPlayer)
		{
			GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            //firstPersonCamera.tag = "Untagged";

            firstPersonCamera.enabled = false;
            firstPersonCamera.tag = "Untagged";

            firstPersonListener.enabled = false;
			player_UI.SetActive(false);
            //respawn button/ method here/manage below from Ref script?

            GameObject sCam = GameObject.Find("GameManager").GetComponent<GameManager_Referances>().sceneCamera;
            sCam.tag = "MainCamera";
            sCam.SetActive(true);

			GameObject.Find ("GameManager").GetComponent<GameManager_Referances>().reSpawnButton.SetActive(true);

		}
	}
}
