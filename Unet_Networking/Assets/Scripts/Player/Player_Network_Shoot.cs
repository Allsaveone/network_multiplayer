﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player_Network_Shoot : NetworkBehaviour {

    public int damage = 25;
    public float range = 200f;
	[SerializeField]public Transform muzzleTransform;
	[SerializeField]public Transform muzzleFlash;

    private RaycastHit hit;
	
	void Update ()
	{
		CheckIfShooting ();
	}

	void CheckIfShooting()
	{
		if(!isLocalPlayer)
		{
			return;
		}

		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			Shoot();
		}

        if (Input.GetKeyDown(KeyCode.K))
        {
            TestDie();
        }
    }

    void TestDie()
    {
        string identity = this.gameObject.name;
        CmdTellServerWhoWasShot(identity, 9999);
        CmdTellServerWhoShotWho(this.gameObject.name, identity);
    }

    [Client]
	void Shoot()
	{
		Debug.Log ("Pew!");
        CmdOnAttack();//other players see me attack

        if (Physics.Raycast(muzzleTransform.TransformPoint(0,0,0.5f), muzzleTransform.forward, out hit, range))
		{
			Debug.DrawRay(muzzleTransform.position,muzzleTransform.forward,Color.red,0.5f);

			if(hit.transform.tag == "Player")
			{
				string identity = hit.transform.name;
				CmdTellServerWhoWasShot(identity,damage);
				CmdTellServerWhoShotWho(this.gameObject.name,identity);
			}
		}

	}

    [Command]//only runs on server
	void CmdTellServerWhoWasShot(string ID, int damage)
	{
		GameObject go = GameObject.Find (ID); //WASTE
		//Apply Damage
		go.SendMessage ("ApplyDamage", damage, SendMessageOptions.RequireReceiver);
	}

	[Command]
	void CmdTellServerWhoShotWho(string ShooterID, string shotID)
	{
		GameObject go = GameObject.Find (shotID);
		//Maybe combine these two Commands....
		go.SendMessage ("IGotHurtBy", ShooterID, SendMessageOptions.RequireReceiver);
	}

    [Command]
    void CmdOnAttack()
    {
        //server pooled shooting fx? 
        RpcAttackFX();//muzzle flash etc
    }

    [ClientRpc]//call on all clients
    void RpcAttackFX()
    {
        //Do Fx
        if(!isLocalPlayer)//3rd person
        muzzleFlash.gameObject.SetActive(true);
    }
}
