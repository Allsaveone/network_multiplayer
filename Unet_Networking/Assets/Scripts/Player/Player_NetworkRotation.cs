﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player_NetworkRotation : NetworkBehaviour {


	[SyncVar] private Quaternion syncPlayerRotation;
	[SyncVar] private Quaternion syncCamRotation;

	[SerializeField]private Transform playerTransform;
	[SerializeField]private Transform camTransform;
	[SerializeField]private float lerpRate = 20f; 

	private Quaternion lastPlayerRot;
	private Quaternion lastCamRot;
	private float threshold = 5f;

	void Update()
	{
		LerpRotations ();

	}

	void FixedUpdate () 
	{
		TransmitRotations ();
	}
	void LerpRotations()
	{
		if(!isLocalPlayer) //if not mine....
		{
			playerTransform.rotation = Quaternion.Lerp (playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
			camTransform.rotation = Quaternion.Lerp (camTransform.rotation, syncCamRotation, Time.deltaTime * lerpRate);
		}
	}

	[Command]
	void CmdProvideRotationsToServer(Quaternion playerRot, Quaternion camRot)
	{
		syncPlayerRotation = playerRot;
		syncCamRotation = camRot;
//		Debug.Log ("CmdProvideRotationsToServer");
	}

	[Client]
	void TransmitRotations()
	{
		if(isLocalPlayer)
		{
			if(Quaternion.Angle(playerTransform.rotation,lastPlayerRot) > threshold ||
			   Quaternion.Angle(camTransform.rotation,lastCamRot) > threshold)
			{
				CmdProvideRotationsToServer(playerTransform.rotation,camTransform.rotation);
				lastPlayerRot = playerTransform.rotation;
				lastCamRot = camTransform.rotation;
			}

//			CmdProvideRotationsToServer(playerTransform.rotation,camTransform.rotation);
//			lastPlayerRot = playerTransform.rotation;
//			lastCamRot = camTransform.rotation;
		}
	}
}
