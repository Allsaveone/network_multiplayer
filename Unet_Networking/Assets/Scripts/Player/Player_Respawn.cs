﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Player_Respawn : NetworkBehaviour {

	[SerializeField]Camera firstPersonCamera;
	[SerializeField]AudioListener firstPersonListener;
	[SerializeField]GameObject firstPersonCharacter;
	public Player_Health hp;
	public GameObject player_UI;
	public GameObject reSpawn_UI;

    public override void PreStartClient()//set references that are also req for remote clients - runs before islocalplayer flag is set- NB
    {
        hp = GetComponent<Player_Health>();
        hp.EventRespawn += EnablePlayer; //Sub to event!
    }

    public override void OnStartLocalPlayer()
    {
		player_UI = GameObject.Find ("PlayerUI").gameObject;
		SetRespawnUI ();
	}

    public override void OnNetworkDestroy()//un sub to stop leaks
	{
		hp.EventRespawn -= EnablePlayer;
	}

	void SetRespawnUI()
	{
		if(isLocalPlayer)
		{
			reSpawn_UI = GameObject.Find ("GameManager").GetComponent<GameManager_Referances>().reSpawnButton;
			reSpawn_UI.GetComponent<Button>().onClick.AddListener(DoRespawn); //adding functions through code!
			reSpawn_UI.SetActive(false);//just incase
		}
	}

	void DoRespawn()
	{
		CmdRespawn ();
		reSpawn_UI.SetActive (false);
	}

	[Command]
	void CmdRespawn()
	{
		hp.ResetHealth ();
	}

	void EnablePlayer () 
	{
		GetComponent<CharacterController>().enabled = true;
		GetComponent<Player_Network_Shoot>().enabled = true;
		GetComponent<CapsuleCollider>().enabled = true;
        //GetComponent<NetworkAnimator>().enabled = true;

        firstPersonCharacter.SetActive(true);

		//hp.isDead = false;

		if(isLocalPlayer)
		{
            //GameObject.Find ("GameManager").GetComponent<GameManager_Referances>().sceneCamera.SetActive(false);
            GameObject sCam = GameObject.Find("GameManager").GetComponent<GameManager_Referances>().sceneCamera;
            sCam.tag = "Untagged";
            sCam.SetActive(false);

            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;

            firstPersonCamera.tag = "MainCamera";
            firstPersonCamera.enabled = true;
			firstPersonListener.enabled = true;
			player_UI.SetActive(true);
			//respawn button/ method here/ choose new spawnPoint
		}
	
	}
}
