﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Player_ID : NetworkBehaviour {

	//[SyncVar] private string playerCharacterName; // later, simular method
	[SyncVar] public string playerUniqueIdenity;
	private NetworkInstanceId playerNetID;
	private Transform myTransform;
    public Text playerName;

    public override void OnStartLocalPlayer()
	{
        Debug.Log("Started Local Player");
		GetIdentity ();
		SetIdenity ();
        //Local specific stuff activated here
	}

	void Awake () 
	{
		myTransform = transform;
	}
	
	void Update () 
	{
		if(myTransform.name == "" || myTransform.name == "Player(Clone)")
		{
			SetIdenity(); // this will only becalled at the endof the players first Frame
		}
	}

	//[Client]
	void SetIdenity()
	{
		if(!isLocalPlayer)
		{
            playerName.text = playerUniqueIdenity;
            myTransform.name = playerUniqueIdenity;
		}
		else
		{
            playerName.text = playerUniqueIdenity;
            playerName.enabled = false;
            //myTransform.name = CreateUniqueId();
            myTransform.name = playerUniqueIdenity;
        }
    }

	[Client]
	void GetIdentity()
	{
		playerNetID = GetComponent<NetworkIdentity> ().netId;
		//CmdTellServerMyIdentity (CreateUniqueId());
	}

	string CreateUniqueId()
	{
		string uniqueName = "Player" + playerNetID.ToString ();
		return uniqueName;
	}

	[Command]
	void CmdTellServerMyIdentity(string name)
	{
		playerUniqueIdenity = name;
	}

}
