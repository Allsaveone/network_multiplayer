﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class Player_Latency : NetworkBehaviour {

    private NetworkClient nClient;
    public int latency;
    public Text latencyText;

    public override  void OnStartLocalPlayer ()
    {
        //nClient = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
        nClient = GameObject.Find("LobbyManager").GetComponent<NetworkManager>().client;

        latencyText = GameObject.Find("LatencyText").GetComponent<Text>();
    }
	
	void Update ()
    {
        ShowLatency();
    }

    void ShowLatency()
    {
        if (isLocalPlayer)
        {
            latency = nClient.GetRTT();
            latencyText.text = latency.ToString();
        }
    }
}
