﻿using UnityEngine;
using System.Collections;

public class BillBoard : MonoBehaviour {
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 v = Camera.main.transform.position - this.transform.position;
        transform.LookAt(Camera.main.transform.position - v);
        transform.rotation = (Camera.main.transform.rotation);
    }
}
