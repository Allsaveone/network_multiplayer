﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Network_Manager : NetworkManager {

    public Button hostButton;
    public Button joinButton;
    public InputField ipInput;

    public Button disconnectButton;

    void Start()
    {
        hostButton = GameObject.Find("Host Button").transform.GetComponent<Button>();
        joinButton = GameObject.Find("Join Button").transform.GetComponent<Button>();
        ipInput = GameObject.Find("IpAddressInputField").transform.GetComponent<InputField>();

        StartCoroutine(AddMenuListeners());
    }

    public void StartUpHost ()
    {
        SetPort();
        NetworkManager.singleton.StartHost();
        //NetworkManager.singleton.
    }

    void SetPort ()
    {
        NetworkManager.singleton.networkPort = 7777;
    }

    void SetPorSetIpAddress()
    {
        string ipAddress = ipInput.text;//GameObject.Find("IpAddressInputField").transform.GetComponent<InputField>().text;
        Debug.Log(ipAddress);
        NetworkManager.singleton.networkAddress = ipAddress;
    }

    public void JoinGame()
    {
        SetPorSetIpAddress();
        SetPort();
        NetworkManager.singleton.StartClient();
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == 0)//menu buttons
        {
            Debug.Log("Lobby");
            hostButton = GameObject.Find("Host Button").transform.GetComponent<Button>();
            joinButton = GameObject.Find("Join Button").transform.GetComponent<Button>();
            ipInput = GameObject.Find("IpAddressInputField").transform.GetComponent<InputField>();

            disconnectButton = null;

            StartCoroutine(AddMenuListeners());
        }
        else//in game buttons
        {
            Debug.Log("InGame");
            hostButton = null;
            joinButton = null;
            ipInput = null;

            disconnectButton = GameObject.Find("Disconnect Button").transform.GetComponent<Button>();
            AddInGameListeners();
        }
    }

    IEnumerator AddMenuListeners()//Slight delay for dontdestroy - 
    {
        yield return new WaitForSeconds(0.1f);
        hostButton.onClick.RemoveAllListeners();
        hostButton.onClick.AddListener(StartUpHost);

        joinButton.onClick.RemoveAllListeners();
        joinButton.onClick.AddListener(JoinGame);
    }

    void AddInGameListeners()
    {
        disconnectButton.onClick.RemoveAllListeners();
        disconnectButton.onClick.AddListener(NetworkManager.singleton.StopHost);
    }
}
