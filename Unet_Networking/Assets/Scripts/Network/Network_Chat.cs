﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;//Req for reading netmesgs
using System.Collections.Generic;

public class Network_Chat : NetworkBehaviour
{

    //Chat History
    public List<string> chatHistory = new List<string>();
    //Keeps track of current message
    private string currentMessage = String.Empty;
    NetworkClient myClient;


    // hook into NetworkManager client setup process
    public override void OnStartLocalPlayer()
    {
        NetworkIdentity id = GetComponent<NetworkIdentity>();
        //base.OnStartClient(mClient); // base implementation is currently empty
        myClient = GameObject.Find("LobbyManager").GetComponent<NetworkManager>().client;

        myClient.RegisterHandler((short)Network_Chat_Msg.MyMessageTypes.CHAT_MESSAGE, OnClientChatMessage);
    }

    // hook into NetManagers server setup process
    public override void OnStartServer()
    {
        base.OnStartServer(); //base is empty
        NetworkServer.RegisterHandler((short)Network_Chat_Msg.MyMessageTypes.CHAT_MESSAGE, OnServerChatMessage);
    }

    private void SendMessage()
    {
        if (string.IsNullOrEmpty(currentMessage.Trim()))
        {
            //GetComponent<NetworkView>().RPC("ChatMessage", RPCMode.AllBuffered, new object[] { currentMessage }); //OBVIOUSLY DOES NOT WORK
            currentMessage = String.Empty;
        }
    }

    private void BottomChat()
    {
        currentMessage = GUI.TextField(new Rect(0, Screen.height - 20, 175, 20), currentMessage);
        if (GUI.Button(new Rect(200, Screen.height - 20, 75, 20), "Send"))
        {
            SendMessage();
        }
        GUILayout.Space(15);
        for (int i = chatHistory.Count - 1; i >= 0; i--)
            GUILayout.Label(chatHistory[i]);
    }

    private void TopChat()
    {
        GUILayout.Space(15);

        GUILayout.BeginHorizontal(GUILayout.Width(250));

        currentMessage = GUILayout.TextField(currentMessage);

        if (GUILayout.Button("Send"))
        {
            if (!string.IsNullOrEmpty(currentMessage))
            {
                Network_Chat_Msg.ChatMessage msg = new Network_Chat_Msg.ChatMessage();
                msg.message = currentMessage;
                NetworkManager.singleton.client.Send((short)Network_Chat_Msg.MyMessageTypes.CHAT_MESSAGE, msg);

                currentMessage = String.Empty;
            }
        }

        GUILayout.EndHorizontal();

        foreach (string c in chatHistory)
            GUILayout.Label(c);
    }

    private void OnServerChatMessage(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<StringMessage>();
        Debug.Log("New chat message on server: " + msg.value);

        Network_Chat_Msg.ChatMessage chat = new Network_Chat_Msg.ChatMessage();
        chat.message = msg.value;

        if (msg.value == "Dam this game sucks")
        {
            return;
        }
        else
        {
            NetworkServer.SendToAll((short)Network_Chat_Msg.MyMessageTypes.CHAT_MESSAGE, chat);
        }
    }

    private void OnClientChatMessage(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<StringMessage>();
        Debug.Log("New chat message on client: " + msg.value);

        chatHistory.Add(msg.value);
    }

    //Chatbox
    private void OnGUI()
    {
        TopChat();
        //BottomChat();
    }

    //[RPC]
    public void ChatMessage(string message)
    {
        chatHistory.Add(message);
    }
}

